//
//  ChatMessages.swift
//  ChatDemoApp
//
//  Created by Sagar Jadhav on 21/08/17.
//  Copyright © 2017 Sagar Jadhav. All rights reserved.
//

import Foundation
import RealmSwift

class ChatMessages: RealmSwift.Object {
    dynamic var key: String = ""
    dynamic var message: String = ""
    dynamic var person: String = ""
    dynamic var messageType: String = ""
    override class func primaryKey() -> String? {
        return "key"
    }
}

class DatabaseOperation {
    static let shared = DatabaseOperation()
    func fetchMessages() -> ([ChatMessages]) {
        let chatMessages = DatabaseCore.shared.retrieveObjects(ChatMessages.self)
        return (chatMessages)
    }
    
    func saveChatMessage(message:String,_ person:String,_ messageType:String ) -> Bool {
        let chat = ChatMessages()
        chat.key = String(Date().timeIntervalSince1970)
        chat.message = message
        chat.person = person
        chat.messageType = messageType
        return DatabaseCore.shared.saveObject(chat)
    }
    
    func deleteChatData() {
        let chat = DatabaseCore.shared.retrieveObjects(ChatMessages.self)
        for ch in chat {
            _ = DatabaseCore.shared.deleteObject(ch)
        }
    }
}

class UserDefaultsOperation {
    static let shared = UserDefaultsOperation()

    func saveUserName(name:String) {
        UserDefaults.standard.set(name , forKey: "UserName")
        UserDefaults.standard.synchronize()
    }
    
    func saveUserEmail(email:String) {
        UserDefaults.standard.set(email , forKey: "UserEmail")
        UserDefaults.standard.synchronize()
    }
    
    func deleteUserName() {
        UserDefaults.standard.removeObject(forKey: "UserName")
        UserDefaults.standard.synchronize()
    }
    
    func getUserNameFor(email:String) -> String? {
        return UserDefaults.standard.object(forKey: email) as? String
    }
    
    func isUserNameStored() -> Bool {
        if (UserDefaults.standard.object(forKey: "UserName") != nil) {
            return true
        } else {
            return false
        }
    }
}
