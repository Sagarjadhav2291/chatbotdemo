/*
 * Copyright 2015 Globant
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import RealmSwift

/// Implementation of application-independent database access logic.
open class DatabaseCore: NSObject {
    
    /// Singleton instance for DatabaseCore
    static let shared: DatabaseCore = DatabaseCore()
    
    /// Realm instance for perfoming the database operation
    fileprivate lazy var realm: Realm? = {
        var realm = try? Realm()
        return realm
    }()
    
    /// Saves a Realm object in database.
    ///
    /// - Parameter object: The Realm Object to be saved.
    open func saveObject(_ object:RealmSwift.Object) -> Bool {
        guard let realm = realm
            else {
                return false
        }
        
        do {
            try realm.write {
                realm.add(object, update: true)
            }
            return true
        }
        catch (let e) {
            print("realm saveObject error: \(e)")
            return false
        }
    }
    
    /// Deletes a Realm object from the database.
    ///
    /// - Parameter object: The Realm Object to be deleted.
    open func deleteObject(_ object: RealmSwift.Object) -> Bool {
        guard let realm = realm
            else {
                return false
        }
        
        do {
            try realm.write {
                realm.delete(object)
            }
            
            return true
        }
        catch (let e) {
            print("realm deleteObject error: \(e)")
            return false
        }
    }
    
    /// Retrieves objects from Realm database.
    ///
    /// - Parameters:
    ///   - type: Class Type of objects to be retrieved.
    ///   - predicate: Predicate to be applied on fetch results.
    /// - Returns: The retrieved objects.
    open func retrieveObjects<T: RealmSwift.Object>(_ type: T.Type, predicate: NSPredicate? = nil) -> Array<T> {
        guard let realm = realm
            else {
                return []
        }
        
        var objects: RealmSwift.Results = realm.objects(type)
        if let l_predicate = predicate {
            objects = objects.filter(l_predicate)
        }
        
        return Array(objects)
    }
}
