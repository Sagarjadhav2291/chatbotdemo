//
//  ViewController.swift
//  ChatDemoApp
//
//  Created by Sagar Jadhav on 21/08/17.
//  Copyright © 2017 Sagar Jadhav. All rights reserved.
//

import UIKit
import ApiAI
class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var questionTextField: UITextField!
    @IBOutlet weak var chatBotLabel: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var isStart:Bool = false
    var array = [ChatMessages]()
    var currentQuestionIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        addKeyboardObserver()
        // Do any additional setup after loading the view, typically from a nib.
        DatabaseOperation.shared.deleteChatData()
        UserDefaultsOperation.shared.deleteUserName()

        array = DatabaseOperation.shared.fetchMessages()
        print(array)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sendButtonClicked(_ sender: UIButton) {
        guard let question = questionTextField.text else { return }
        if question.isEmpty {
            let alert = UIAlertController(title: "", message: "Please type message", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }  else {
                askQuestionToBot(string: question)
            questionTextField.text = ""
            _ = DatabaseOperation.shared.saveChatMessage(message: question, "USER", "text")
            reload()
        }
        

    }
    
    
    func askQuestionToBot(string:String) {

        let successHandler = {
            [weak self] (result:[[AnyHashable : Any]], action: String?) in
            print(result)
            guard let strongSelf = self else { return }

            let filteredResultForCustomPayload = result.filter({ (item) -> Bool in
                return item["type"] as? Int == 4
            })
            let filteredResultDefaultSpeech = result.filter({ (item) -> Bool in
                return item["type"] as? Int == 0
            })
            
            if let responseObj = filteredResultForCustomPayload.first {
                if responseObj["type"] as? Int == 4 {
                    guard var payload:Dictionary<String,AnyObject> = responseObj["payload"] as? Dictionary<String,AnyObject> else {
                        return
                    }
                    guard let messageType:String = payload["message_type"] as? String else {
                        return
                    }

                    
                    if var chatbotString:String = payload["message"] as? String {
                        if messageType.contains("text") {
                            if let userName:String = UserDefaults.standard.value(forKey: "UserName") as? String
                            {
                                chatbotString = chatbotString.replacingOccurrences(of: "<USER>", with: "\(userName)")
                            }
                        }
                        strongSelf.chatBotLabel.text = chatbotString
                        _ = DatabaseOperation.shared.saveChatMessage(message: chatbotString, "CHATBOT", messageType)
                       strongSelf.reloadAfterResponse()
                    }
                    if var linkStringArray:[String] = payload["message"] as? [String] {
                        
                    }
                    
                    
                    
                    
                    
                    
                }
            }
            else{

                if let responseObj = filteredResultDefaultSpeech.first {
                    guard let chatbotString:String = responseObj["speech"] as? String else {
                        return
                    }
                    strongSelf.chatBotLabel.text = chatbotString
                    _ = DatabaseOperation.shared.saveChatMessage(message: chatbotString, "CHATBOT", "text")
                    strongSelf.reloadAfterResponse()
                }
            }

        }
        
        let failureHandler = {
            (error:Error?) in
            print(error?.localizedDescription ?? "Error")
        }
        
        print("askQuestionToBot : \(string)")
        callAPI(queryText: string, success: successHandler, failure: failureHandler)
    }

    
    
    
    
    
    
    func callAPI(queryText:String,
                 success:@escaping (_ response:[[AnyHashable:Any]],_ action:String?) -> Void,
                         failure:@escaping (_ error:Error?) -> Void) {
        let request = ApiAI.shared().textRequest()
        
        request?.query = [queryText]
        
        request?.setCompletionBlockSuccess({
            (request, response) -> Void in
            let aiResponse = AIResponse(response: response)
            
            guard let fulfillment = aiResponse?.result.fulfillment else{
                success([["speech":"Sorry! I could not understand","type":0]], nil)
                return
            }
            
            guard let messages:[[AnyHashable:Any]] = fulfillment.messages else{
                success([["speech":"Sorry! I could not understand","type":0]], nil)
                return
            }
            guard let action = aiResponse?.result.action else {
                return
            }

            if messages.count > 0 {
                success(messages, action)
            }
        }, failure: { (request, error) -> Void in
            print(" Error : \(String(describing: error?.localizedDescription))")
            failure(error)
        });
        
        ApiAI.shared().enqueue(request)
    }
    
    func reload() {
        self.array = DatabaseOperation.shared.fetchMessages()
        self.tableView.reloadData()
        
        DispatchQueue.main.async(execute: { [weak self]() -> Void in
            guard let strongSelf = self else { return }
            let indexPath = IndexPath(row: strongSelf.array.count-1, section: 0)
            strongSelf.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
        })
        
        
//        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
//
//        DispatchQueue.main.asyncAfter(deadline: when) {
//            print("reload")
//
//            print("self.tableView.contentSize.height\(self.tableView.contentSize.height)")
//            print("self.tableView.frame.size.height\(self.tableView.frame.size.height)")
//            print(self.tableView.contentSize.height - self.tableView.bounds.size.height)
//
//            if self.tableView.contentSize.height > self.tableView.frame.size.height {
//                let bottomOffset = CGPoint(x: 0, y: self.tableView.contentSize.height - self.tableView.frame.size.height)
//                self.tableView.setContentOffset(bottomOffset, animated: true)
//
//            }
//            
//        }
    }
    
    func reloadAfterResponse() {
        self.array = DatabaseOperation.shared.fetchMessages()
        self.tableView.reloadData()
        
        DispatchQueue.main.async(execute: { [weak self]() -> Void in
            guard let strongSelf = self else { return }
            let indexPath = IndexPath(row: strongSelf.array.count-1, section: 0)
            strongSelf.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
        })
        
//        let when = DispatchTime.now() + 3 // change 2 to desired number of seconds
//        DispatchQueue.main.asyncAfter(deadline: when) {
//            print("reloadAfterResponse")
//            print("self.tableView.contentSize.height\(self.tableView.contentSize.height)")
//            print("self.tableView.frame.size.height\(self.tableView.frame.size.height)")
//            print(self.tableView.contentSize.height - self.tableView.frame.size.height)
//
//            if self.tableView.contentSize.height > self.tableView.frame.size.height {
//                let bottomOffset = CGPoint(x: 0, y: (self.tableView.contentSize.height - self.tableView.frame.size.height))
//                self.tableView.setContentOffset(bottomOffset, animated: true)
//
//            }
//            let indexPath = IndexPath(row: self.array.count-1, section: 0)
//            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
//
//        }
        
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let chat = array[indexPath.row]
        if chat.person == "CHATBOT" {
            if chat.messageType == "links" {
                return 150
            }
            if chat.messageType == "text-links" {
                return 180
            }
        }
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chat = array[indexPath.row]
        if chat.person == "USER" {
            guard let cell: UserTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as? UserTableViewCell
                else{
                    return UITableViewCell(style: .default, reuseIdentifier: nil)
            }
            cell.titleLabel.text =  chat.message
            return cell
        } else {
            if chat.messageType == "links" {
                
                
                if chat.message == "offer;offer;offer" {
                    guard let cell: OffersTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OffersTableViewCell", for: indexPath) as? OffersTableViewCell
                        else{
                            return UITableViewCell(style: .default, reuseIdentifier: nil)
                    }
                    cell.collectionView.delegate = self
                    cell.collectionView.dataSource = self
                    cell.collectionView.tag = 200

                    return cell
                } else {
                    guard let cell: CapabilityTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CapabilityTableViewCell", for: indexPath) as? CapabilityTableViewCell
                        else{
                            return UITableViewCell(style: .default, reuseIdentifier: nil)
                    }
                    cell.collectionView.delegate = self
                    cell.collectionView.dataSource = self
                    cell.collectionView.tag = 300

                    return cell

                }
                

            }
            else if chat.messageType == "text-links" {
                guard let cell: PieChartTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PieChartTableViewCell", for: indexPath) as? PieChartTableViewCell
                    else{
                        return UITableViewCell(style: .default, reuseIdentifier: nil)
                }
                cell.collectionView.delegate = self
                cell.collectionView.dataSource = self
                cell.collectionView.tag = 100 //pie chart
                cell.titleLabel.text = chat.message
                return cell
            }
            else {
                guard let cell: ChatBotTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChatBotTableViewCell", for: indexPath) as? ChatBotTableViewCell
                    else{
                        return UITableViewCell(style: .default, reuseIdentifier: nil)
                }
                cell.titleLabel.text =  chat.message
                return cell

            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension ViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
    }
    
    @objc func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100 {
            return 1
        }
        return 3
        
    }
    
    internal func collectionView(_ collectionView: UICollectionView,
                                cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 100 {
            guard let cell:CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell
                else{
                    return UICollectionViewCell()
            }
             cell.image.image = UIImage(named: "PieChart")
            return cell
        
        }else  if collectionView.tag == 200 {
            guard let cell:CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell
                else{
                    return UICollectionViewCell()
            }
            cell.button.addTarget(self, action:#selector(buttonClicked), for: .touchUpInside)
            cell.image.image = UIImage(named: "Offer\(indexPath.row)")
            return cell
        } else {
            guard let cell:CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell
                else{
                    return UICollectionViewCell()
            }
            cell.button.addTarget(self, action:#selector(buttonClicked), for: .touchUpInside)
            cell.image.image = UIImage(named: "Job\(indexPath.row)")
            return cell
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 150, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
    }

    func buttonClicked(sender:UIButton){
        UIApplication.shared.open(URL(string: "http://www.google.com")!, options: [:], completionHandler: nil)

    }

}

class ChatBotTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //cellView.backgroundColor = UIColor.gray
        cellView.layer.cornerRadius = 5.0
        cellView.layer.borderColor = UIColor.darkGray.cgColor
        cellView.layer.borderWidth = 1.0
    }
}

class OffersTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
}
class CapabilityTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
}


class PieChartTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //cellView.backgroundColor = UIColor.gray
        cellView.layer.cornerRadius = 5.0
        cellView.layer.borderColor = UIColor.darkGray.cgColor
        cellView.layer.borderWidth = 1.0
    }
}

class UserTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var img: UIImageView!

    override func layoutSubviews() {
        super.layoutSubviews()
        // cellView.backgroundColor = UIColor.blue
        cellView.layer.cornerRadius = 5.0
        cellView.layer.borderColor = UIColor.darkGray.cgColor
        cellView.layer.borderWidth = 1.0
        img.clipsToBounds = true
        img.layer.cornerRadius = 10.0

    }
}

class CollectionViewCell: UICollectionViewCell{
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var button: UIButton!

}

extension ViewController {
    func addKeyboardObserver(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
    }
    
    
    func keyboardWillShow(notification: Notification) {
        var info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.bottomConstraint?.constant = keyboardFrame.size.height + 20
            self.view.layoutIfNeeded()
        })
    }
    
    func keyboardWillHide(notification: Notification){
        
        var info = notification.userInfo!
        let duration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.bottomConstraint?.constant = 0
            self.view.layoutIfNeeded()
        })
    }
}

